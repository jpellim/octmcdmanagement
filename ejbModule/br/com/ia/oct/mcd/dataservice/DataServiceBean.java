package br.com.ia.oct.mcd.dataservice;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;

/**
 * Componente utilizado para acesso aos dados provenientes da base OCT-MCD
 * Atua como um DAO, por�m funciona como um EJB
 * @author fernandosapata
 *
 * @param <T>
 */
@Stateless
@Remote(DataService.class)
@Local(DataServiceLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/management/dataservice")
@EJB(name = "octopus/mcd/management/dataservice", beanInterface = DataService.class)
@OCTComponent(version="$LastChangedRevision$",uid="4.3.2", nome="DataServiceBean", descricao="Componente de acesso a base de dados da plataforma Octopus", jndiLookup="MCD/DataServiceBean/local", clazz="", cronTrigger="0 0/5 * 1/1 * ? *", grupo="MANAGEMENT", ativo=true)
public class DataServiceBean<T> implements DataServiceLocal<T>, DataService<T> {
	private static final Logger LOG = Logger.getLogger(DataServiceBean.class);
	private Class<? extends T> type;
	
	@PersistenceContext
	private EntityManager manager;
	private Session session;
	
	public DataServiceBean () {
	}
	
	public T getByID(Serializable id, Class type) {
		this.session = (Session) manager.getDelegate();
		this.type = type;
		return (T) session.get(type, id);
	}
	
	public List<T> getList (Class type) {
		this.session = (Session) manager.getDelegate();
		this.type = type;
		return session.createCriteria(type)
				.list();
	}
	
	public void save (T obj, Class type) {
		try {
			this.session = (Session) manager.getDelegate();
			session.beginTransaction();
			session.save(obj);
			session.flush();
		} catch (Throwable t) {
			LOG.error(t.getMessage(), t);
		} 
	}

	public void evict (T obj) {	
		this.session = (Session) manager.getDelegate();
		//session.beginTransaction();
		session.clear();
		session.evict(obj);
		//session.flush();
	}	
	
	public T saveObj (T obj, Class type) {
		this.session = (Session) manager.getDelegate();
		
		this.session.save(obj);
		
		Long id = (Long) this.session.getIdentifier(obj);
		
		Object object = this.session.get(type, id);
		
		return (T) object;		
	}
	
	public void saveOrUpdate (T obj, Class type) {
		this.session = (Session) manager.getDelegate();
		session.beginTransaction();
		session.saveOrUpdate(obj);
		session.flush();
		session.getTransaction().commit(); 
	}

	public Session getSession() {
		return this.session;
	}

	public EntityManager getEntityManager() {
		return this.manager;
	}

	public List<T> getList(Class type, int maxResult) {
		this.session = (Session) manager.getDelegate();
		this.type = type;
		return session.createCriteria(type)
				.setMaxResults(maxResult)
				.list();
	}

	/**
	 * Utilize esta m�todo para retornar a lista de itens de uma determinada entidade utilizando o parametro de Order
	 * Caso queira retornar todos os registros utilize o valor 0 no mar�metro maxResult
	 */
	public List<T> getList(Class type, int maxResult, Order order) {
		this.session = (Session) manager.getDelegate();
		this.type = type;
		Criteria criteria = session.createCriteria(type).addOrder(order);
				
		if (maxResult > 0) {
			criteria.setMaxResults(maxResult);
		}
		
		return criteria.list();
	}

	public List<T> getList(Class type, Junction junction) {
		this.session = (Session) manager.getDelegate();
		this.type = type;
		return session.createCriteria(type)
				.add(junction)
				.list();
	}

	public Boolean isAlive() {

		return true;
	}

	public List<T> getList(Class type, int maxResult, Order order, Junction junction) {
		this.session = (Session) manager.getDelegate();
		this.type = type;

		Criteria criteria = session.createCriteria(type)
				.add(junction)
				.addOrder(order);
		
		if (maxResult > 0) {
			criteria.setMaxResults(maxResult);
		}
		
		return criteria.list();
	}

	public List<T> getList(Class type, Order order, Junction junction) {
		this.session = (Session) manager.getDelegate();
		this.type = type;

		Criteria criteria = session.createCriteria(type)
				.add(junction)
				.addOrder(order);
		
		return criteria.list();
	}

	public List executeQuery(String nativeQuery) {
		this.session = (Session) manager.getDelegate();
		
		return session.createSQLQuery(nativeQuery).list();
	}	
	
	public Integer executeUpdateQuery(String nativeQuery) {
		this.session = (Session) manager.getDelegate();
		
		return session.createSQLQuery(nativeQuery).executeUpdate();
	}		
}
