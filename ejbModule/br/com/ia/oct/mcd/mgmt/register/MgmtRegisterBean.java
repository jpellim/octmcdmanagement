package br.com.ia.oct.mcd.mgmt.register;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Restrictions;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.annotations.OCTJob;
import br.com.ia.oct.mcd.common.entity.Component;
import br.com.ia.oct.mcd.common.entity.Job;
import br.com.ia.oct.mcd.dataservice.DataService;
import br.com.ia.oct.mcd.dataservice.DataServiceBean;
import br.com.ia.oct.mcd.infrastructure.trace.TraceServiceBean;
import br.com.ia.oct.mcd.remedy.CategorizationServiceImpl;

@Stateful
@Remote(MgmtRegister.class)
//@RemoteBinding(jndiBinding="octopus/mcd/management/register")
@EJB(name = "octopus/mcd/management/register", beanInterface = MgmtRegister.class) 
public class MgmtRegisterBean implements MgmtRegister {
	private static final Logger LOG = Logger.getLogger(MgmtRegisterBean.class);
	
	//@EJB(beanInterface=DataService.class)
	@Inject
	private DataService dataService;
	
	public void doRegister() throws Exception {
		this.registerComponent(DataServiceBean.class);
		this.registerComponent(TraceServiceBean.class);
		this.registerComponent(CategorizationServiceImpl.class);
	}

	public void registerJob(Class clazz) {		
		if (clazz.isAnnotationPresent(OCTJob.class)) {			
			OCTJob octJob = (OCTJob) clazz.getAnnotation(OCTJob.class);
			Job job;
			
			Junction restrictions = Restrictions.conjunction();
			restrictions.add(Restrictions.eq("jndiLookup", octJob.jndiLookup()));
					
			List<Job> jobs = dataService.getList(Job.class, restrictions);
			
			// Registrando novo componente
			if (jobs.size() == 0) {
				job = new Job();
				
				job.setProduto(octJob.produto());
				job.setAtivo(octJob.ativo());
				job.setClazz(octJob.clazz());
				job.setCronTrigger(octJob.cronTrigger());
				job.setDescricao(octJob.descricao());
				job.setGrupo(octJob.grupo());
				job.setJndiLookup(octJob.jndiLookup());
				job.setNome(octJob.nome());
				job.setNode(octJob.node());
				job.setUid(octJob.uid());
				
				dataService.save(job, Job.class);
				
				LOG.info("Job Registrado com Sucesso [nome=" + job.getNome() + ",jndiLookup=" + job.getJndiLookup() + "]");
			} else {
				job = jobs.get(0);
				job.setAtivo(octJob.ativo());
				job.setClazz(octJob.clazz());
				job.setCronTrigger(octJob.cronTrigger());
				job.setDescricao(octJob.descricao());
				job.setGrupo(octJob.grupo());
				job.setJndiLookup(octJob.jndiLookup());
				job.setProduto(octJob.produto());
				job.setNome(octJob.nome());
				job.setNode(octJob.node());
				job.setUid(octJob.uid());
				dataService.evict(Job.class);
				dataService.saveOrUpdate(job, Job.class);

				List<Job> jobsAtualizados = dataService.getList(Job.class, restrictions);
				for (Job jobAtualizado : jobsAtualizados) {
					System.out.println("Descrição: " + jobAtualizado.getDescricao() + "\n" +
							"CronTrigger: " + jobAtualizado.getCronTrigger());
				}
				
				LOG.info("Job atualizado com sucesso [id=" + job.getId() + ",nome=" + job.getNome() + ",jndiLookup=" + job.getJndiLookup() + "]");
			}
			
			this.registerComponent(clazz);			
		}
	}

	public void registerComponent(Class clazz) {
		if (clazz.isAnnotationPresent(OCTComponent.class)) {
			OCTComponent octComponent = (OCTComponent) clazz.getAnnotation(OCTComponent.class);
			Component component;
			
			Junction restrictions = Restrictions.conjunction();
			restrictions.add(Restrictions.eq("jndiLookup", octComponent.jndiLookup()));
					
			List<Component> components = dataService.getList(Component.class, restrictions);
			
			// Registrando novo componente
			if (components.size() == 0) {
				component = new Component();
				
				component.setProduto(octComponent.produto());
				component.setAtivo(octComponent.ativo());
				component.setClazz(octComponent.clazz());
				component.setCronTrigger(octComponent.cronTrigger());
				component.setDescricao(octComponent.descricao());
				component.setGrupo(octComponent.grupo());
				component.setJndiLookup(octComponent.jndiLookup());
				component.setNome(octComponent.nome());
				component.setUid(octComponent.uid());
				String version = octComponent.version().replace("$LastChangedRevision:", "").replace("$", "").trim();
				component.setcVersion(version);
				
				dataService.save(component, Component.class);
				
				LOG.info("Componente Registrado com Sucesso [nome=" + component.getNome() + ",jndiLookup=" + component.getJndiLookup() + "]");
			} else {
				component = components.get(0);
				String version = octComponent.version().replace("$LastChangedRevision:", "").replace("$", "").trim();
				component.setcVersion(version);
				component.setProduto(octComponent.produto());
				component.setAtivo(octComponent.ativo());
				component.setClazz(octComponent.clazz());
				component.setCronTrigger(octComponent.cronTrigger());
				component.setDescricao(octComponent.descricao());
				component.setGrupo(octComponent.grupo());
				component.setJndiLookup(octComponent.jndiLookup());
				component.setNome(octComponent.nome());
				component.setUid(octComponent.uid());
				
				dataService.evict(Component.class);
				dataService.saveOrUpdate(component, Component.class);
				
				LOG.info("Componente já registrado [id=" + component.getId() + ",nome=" + component.getNome() + ",jndiLookup=" + component.getJndiLookup() + "]");
			}			
		}		
	}

	public void truncateComponents() {
		dataService.executeUpdateQuery("TRUNCATE TABLE [BR_TD_ARCOS].[dbo].[TB_OCT_MCD_JOB_DEFINITIONS]");
		dataService.executeUpdateQuery("TRUNCATE TABLE [BR_TD_ARCOS].[dbo].[TB_OCT_MCD_COMPONENT_DEFINITIONS]");
	}

}
