package br.com.ia.oct.mcd.scheduler;

import java.util.Calendar;
import java.util.HashMap;

import javax.naming.InitialContext;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.ia.oct.mcd.common.monitoring.OCTComponent;
import br.com.ia.oct.mcd.common.entity.Component;
import br.com.ia.oct.mcd.dataservice.DataService;

/**
 * Classe utilizada para executar um componente remoto e coletar estat�sticas de monitora��o
 * � necess�rio que o EJB remoto implemente a interface OCTComponent
 * @author fernandosapata
 *
 */
public class MonitorJob implements Job {
	private static final Logger LOG = Logger.getLogger(MonitorJob.class);
	private DataService service;
	private Component component;
	long inicio = 0L;
	long total = 0L;
			
	public MonitorJob () {
		try {
			InitialContext ctx = new InitialContext();
			service = (DataService) ctx.lookup("octopus/mcd/management/dataservice");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {	
		try {
			JobDetail jobDetail   = context.getJobDetail();
			JobDataMap jobDataMap = jobDetail.getJobDataMap();
			String jndi = (String) jobDataMap.get("jndi");
			Long componentID = (Long) jobDataMap.get("componentID");
			component = (Component) service.getByID(componentID, Component.class);
			LOG.info("Realizando a monitoração do componente [id=" + component + ",jndi=" + component.getJndiLookup() + ", cronTrigger= " +component.getCronTrigger() + "]");
			InitialContext ic = new InitialContext();
			inicio = System.currentTimeMillis();
			
			try {
				OCTComponent job = (OCTComponent) ic.lookup(jndi);				
				Boolean alive = job.isAlive();
				total = System.currentTimeMillis() - inicio;

				if (alive) {
					component.setDuration(total);
					component.setStatus("OK");
					component.setLastPingTime(Calendar.getInstance());
					component.setLastErrorMessage("");
				} else {
					component.setDuration(total);
					component.setStatus("ERROR");
					component.setLastPingTime(Calendar.getInstance());							
				}				
			} catch (Exception e) {
				LOG.info("Exception ao realizar lookup de componente. Mensagem: " + e.getMessage());
			}

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			
			total = System.currentTimeMillis() - inicio;

			component.setDuration(total);
			component.setStatus("ERROR");
			component.setLastPingTime(Calendar.getInstance());										
			component.setLastErrorMessage(e.getMessage());
		} finally {
			service.saveOrUpdate(component, Component.class);
		}
	}

}
