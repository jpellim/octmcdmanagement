package br.com.ia.oct.mcd.scheduler;

import java.util.Calendar;
import java.util.HashMap;

import javax.ejb.EJB;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.ia.oct.mcd.dataservice.DataService;
import br.com.ia.oct.mcd.infrastructure.trace.TraceService;
import br.com.ia.oct.mcd.scheduler.OCTJob;

/**
 * Classe utilizada para executar um job remotamente
 * � necess�rio que o EJB remoto implemente a interface OCTJob
 * @author fernandosapata
 *
 */
public class RemoteJob implements Job {
	private static final Logger LOG = Logger.getLogger(RemoteJob.class);
	private DataService service;
	private TraceService traceService;
	
	public RemoteJob () {
		try {
			InitialContext ctx = new InitialContext();
			service = (DataService) ctx.lookup("octopus/mcd/management/dataservice");
			traceService = (TraceService) ctx.lookup("octopus/mcd/management/infrastructure/traceservice");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {	
		try {			
			JobDetail jobDetail   = context.getJobDetail();
			JobDataMap jobDataMap = jobDetail.getJobDataMap();

			HashMap<String, Object> params = new HashMap<String, Object>();

			String jndi = (String) jobDataMap.get("jndi");
			Long   id   = (Long) jobDataMap.get("id");

			// Grava a �ltima execu��o do JOB
			br.com.ia.oct.mcd.common.entity.Job j = (br.com.ia.oct.mcd.common.entity.Job) service.getByID(id, br.com.ia.oct.mcd.common.entity.Job.class);
			j.setLastExecution(Calendar.getInstance());
			service.saveOrUpdate(j, br.com.ia.oct.mcd.common.entity.Job.class);
			
			traceService.trace(null, j.getUid(), "Iniciando execução do Job [" + j.getUid() + "]", j.getNome(), Calendar.getInstance());
			long inicio = System.currentTimeMillis();
			
			// Executa efetivamente o JOB
			try {
				InitialContext ic = new InitialContext();
				OCTJob job = (OCTJob) ic.lookup(jndi);
				job.execute(params);
				j.setLastExecutionStatus("SUCCESS");
				j.setLastErrorMessage("");
				traceService.trace(null, j.getUid(), "Execução do Job [" + j.getUid() + "] finalizada com status SUCCESS", "", Calendar.getInstance());
			} catch (Exception e) {
				j.setLastExecutionStatus("ERROR");
				j.setLastErrorMessage(e.getMessage());
				LOG.error(e.getMessage(), e);
				traceService.trace(null, j.getUid(), "Execução do Job [" + j.getUid() + "] finalizada com status ERROR", e.getMessage(), Calendar.getInstance());
			} finally {
				Long total = System.currentTimeMillis() - inicio;
				j.setLastExecutionTime(total);
				
				// Proxima execu��o
				Calendar c = Calendar.getInstance();
				
				if (context.getNextFireTime() != null) {
					c.setTime(context.getNextFireTime());	
				}
				
				j.setNextExecution(c);
				
				service.saveOrUpdate(j, br.com.ia.oct.mcd.common.entity.Job.class);
				
				traceService.trace(null, j.getUid(), "Statistics [" + j.getNome() + "]", total + "", Calendar.getInstance());
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

}
