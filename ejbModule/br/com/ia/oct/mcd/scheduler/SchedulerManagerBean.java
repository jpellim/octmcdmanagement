package br.com.ia.oct.mcd.scheduler;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import br.com.ia.oct.mcd.common.entity.Component;
import br.com.ia.oct.mcd.common.entity.Job;
import br.com.ia.oct.mcd.common.utils.Configuration;
import br.com.ia.oct.mcd.common.utils.OCTUtils;
import br.com.ia.oct.mcd.dataservice.DataService;

@Stateful
@Remote(SchedulerManager.class)
@Local(SchedulerManagerLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/management/scheduler/schedulermanager")
//@EJB(name = "java:global/octopus/mcd/management/scheduler/schedulermanager", beanInterface = SchedulerManager.class)
@EJB(name = "java:global/SchedulerManagerBean", beanInterface = SchedulerManager.class)
public class SchedulerManagerBean implements SchedulerManager, SchedulerManagerLocal {
	private static final Logger LOG = Logger.getLogger(SchedulerManagerBean.class);
	private static Scheduler scheduler;
	private static Integer totalJobs = 0;
	private static Integer totalJobsActive = 0;
	private static Integer totalComponents = 0;
	private static Integer totalComponentsActive = 0;
	private static String  schedulerStatus = "{SHUTDOWN}";
	private static String  hostNode01;
	private static String  hostNode02;
	private static String  hostNode03;
	private static String  hostname;
	
	//@EJB  //(mappedName="octopus/mcd/management/dataservice")
	@Inject
	private DataService dataService;
	
	public SchedulerManagerBean () {
		LOG.debug("Construindo componente SchedulerManagerBean...");
		
		hostNode01 = Configuration.getConfig("mcd.nodes.node01");
		hostNode02 = Configuration.getConfig("mcd.nodes.node02");
		hostNode03 = Configuration.getConfig("mcd.nodes.node03");
		hostname   = OCTUtils.getHostname();
	}

	@PostConstruct
	public void inicializa () {
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
			LOG.info("Scheduler retornado com sucesso.");
			schedulerStatus = "{CONSTRUIDO}";
		} catch (Exception e) {
			LOG.fatal(e.getMessage(), e);
		}		
	}
	
	public boolean startup() {
		try {
			if (scheduler.isShutdown()) {
				this.inicializa();
			}
			LOG.info("Inicializando Scheduler...");
			scheduler.start();
			schedulerStatus = "{STARTED}";
			LOG.info("Scheduler inicialiado com sucesso.");
			this.loadJobs();
		} catch (Exception e) {
			LOG.fatal(e.getMessage(), e);
			schedulerStatus = "{" + e.getMessage() + "}";
			return false;
		}
		
		return true;
	}

	public boolean shutdown() {
		try {
			LOG.info("Finalizando Scheduler...");
			scheduler.shutdown();
			LOG.info("Scheduler finalizado com sucesso.");
			schedulerStatus = "{SHUTDOWN}";
		} catch (Exception e) {
			LOG.fatal(e.getMessage(), e);
			schedulerStatus = "{" + e.getMessage() + "}";
			return false;
		}
		
		return true;
	}

	public boolean reload() {
		LOG.info("Iniciando reload do scheduler...");
		try {
			this.shutdown();
			
			this.inicializa();
			
			this.startup();
		} catch (Exception e) {
			LOG.fatal(e.getMessage(), e);
			return false;
		}
		
		return true;
	}
	
	public Boolean isCorrectNode (String node) {
		if ("node01".equalsIgnoreCase(node)) {
			if (hostname.equalsIgnoreCase(hostNode01)) {
				return true;
			}			
		}
		if ("node02".equalsIgnoreCase(node)) {
			if (hostname.equalsIgnoreCase(hostNode02)) {
				return true;
			}			
		}
		if ("node03".equalsIgnoreCase(node)) {
			if (hostname.equalsIgnoreCase(hostNode03)) {
				return true;
			}			
		}
		
		return false;
	}
	
	/**
	 * Carrega a lista de JOBS que est�o salvos no BD e agenda no SchedulerManager
	 */
	private void loadJobs () {
		totalJobsActive = 0;
		totalComponentsActive = 0; 
		
		LOG.info("Retornando lista de Jobs do BD...");
		List<Job> jobs = dataService.getList(Job.class);
		totalJobs = jobs.size();
		
		LOG.info("Total de JOBs para tratamento [total=" + jobs.size() + "]");
		
		for (Job job : jobs) {
			String node = job.getNode();
			
			LOG.info("Verificando JOB [id=" + job.getId() + ", nome=" + job.getNome() + 
					", clazz=" + job.getClazz() + ", jndi=" + job.getJndiLookup() + ", status=" + job.getAtivo() + ",node=" + job.getNode() + ",isCorrectNode=" + isCorrectNode(node) + "]");
			
			if (isCorrectNode(node)) {
				if (job.getAtivo()) {
					totalJobsActive++;
					this.scheduleJob(job);
				}				
			}
		}
		
		LOG.info("Retornando lista de Componentes do BD...");
		
		List<Component> components = dataService.getList(Component.class);
		
		totalComponents = components.size();
		
		LOG.info("Total de Components para tratamento [total=" + components.size() + "]");
		
		for (Component component : components) {
			LOG.info("Verificando Component [id=" + component.getId() + ", nome=" + component.getNome() + 
					", clazz=" + component.getClazz() + ", jndi=" + component.getJndiLookup() + ", status=" + component.getAtivo() + "]");
			
			if (component.getAtivo()) {
				totalComponentsActive++;
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("componentID", component.getId());
				
				this.scheduleJob(component.getId(),component.getJndiLookup(), "McD-MONITORING-" + component.getGrupo(), "MONITORING-" + component.getNome() + " [" + component.getUid() + "]", component.getDescricao(), component.getCronTrigger(), params);
			}
		}	
		
		schedulerStatus = "{totalJobs=" + totalJobs +",totalJobsActive=" + totalJobsActive +",totalComponents=" + totalComponents + ",totalComponentsActive=" + totalComponentsActive + "}";
	}

	public boolean scheduleJob(Long id, String jndi, String grupo, String nome, String descricao, String cronTrigger) {
		try {
			JobDetail job = new JobDetail(nome, grupo, RemoteJob.class);
			
			JobDataMap jobDataMap = new JobDataMap();
			jobDataMap.put("jndi", jndi);
			jobDataMap.put("id", id);
			job.setJobDataMap(jobDataMap);

			CronTrigger trigger = new CronTrigger(nome, grupo, cronTrigger);

			scheduler.scheduleJob(job, trigger);	
			
			LOG.info("JOB  Schedulado com sucesso [id=" + id + ", nome=" + nome + ", jndi=" + jndi + ", cronTrigger=" + cronTrigger + "]");
		} catch (Exception e) {
			LOG.fatal(e.getMessage(), e);
			return false;			
		}
		
		return true;
	}

	public boolean execJobNow(String uid) {
		try {
			Job j = (Job) dataService.getByID(Long.valueOf(uid), Job.class);
			
			JobDetail job = new JobDetail("UNIQUE-" + j.getNome(), "UNIQUE-" + j.getGrupo(), RemoteJob.class);
			
			JobDataMap jobDataMap = new JobDataMap();
			jobDataMap.put("jndi", j.getJndiLookup());
			jobDataMap.put("id", j.getId());
			job.setJobDataMap(jobDataMap);

			Trigger trigger = new SimpleTrigger("UNIQUE-T-" + j.getNome(), 
					"UNIQUE-T-" + j.getGrupo(), 
					new Date(), 
					null, 
					0, 0);

			scheduler.scheduleJob(job, trigger);	
			
			LOG.info("JOB  Schedulado com sucesso [id=" + j.getId() + ", nome=" + j.getNome() + ", jndi=" + j.getJndiLookup() + "]");
			
			return true;
		} catch (Exception e) {
			LOG.fatal(e.getMessage(), e);
			return false;			
		}		
	}	
	
	public boolean scheduleJob(Job job) {
		return scheduleJob(job.getId(),job.getJndiLookup(), job.getGrupo(), job.getNome(), job.getDescricao(), job.getCronTrigger());
	}

	public boolean scheduleJob(Long id, String jndi, String grupo, String nome, String descricao, String cronTrigger, HashMap<String, Object> params) {
		try {
			JobDetail job = new JobDetail(nome, grupo, MonitorJob.class);
			
			JobDataMap jobDataMap = new JobDataMap();
			jobDataMap.put("jndi", jndi);
			jobDataMap.put("id", id);
			
			Iterator<String> iterator = params.keySet().iterator();
			
			while (iterator.hasNext()) {
				String key = iterator.next();
				Object valor = params.get(key);
				jobDataMap.put(key, valor);
				LOG.debug("Adicionando parâmetros adicionais ao JOB [chave=" + key + ",valor=" + valor + "]");
			}
			
			job.setJobDataMap(jobDataMap);

			CronTrigger trigger = new CronTrigger(nome, grupo, cronTrigger);

			scheduler.scheduleJob(job, trigger);	
			
			LOG.info("JOB  Schedulado com sucesso [id=" + id + ", nome=" + nome + ", jndi=" + jndi + ", cronTrigger=" + cronTrigger + "]");
		} catch (Exception e) {
			LOG.fatal(e.getMessage(), e);
			return false;			
		}
		
		return true;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public String shcedulerManagerStatus() {
		return schedulerStatus;
	}
}
