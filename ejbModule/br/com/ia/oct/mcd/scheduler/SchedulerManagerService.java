package br.com.ia.oct.mcd.scheduler;


public interface SchedulerManagerService {
	public boolean startup ();
	public boolean shutdown ();
	public boolean reload ();
	public boolean execJobNow(String uid);
	public boolean scheduleJob (Long id, String jndi, String grupo, String nome, String descricao, String cronTrigger, boolean persistent);
	public boolean updateJobStatus (Long id, boolean active);
	public void start () throws Exception;
	public void destroy () throws Exception;	
	public String getSchedulerManagerStatus();
	public String jobList();
	public String jobListByProduto(String produto);
	public String jobsFireTime() throws Exception;
}
