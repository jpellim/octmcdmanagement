package br.com.ia.oct.mcd.scheduler;

import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Restrictions;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;

import br.com.ia.oct.mcd.common.entity.Job;
import br.com.ia.oct.mcd.dataservice.DataService;
import br.com.ia.oct.mcd.jmx.wrapper.JMXBean;
import br.com.ia.oct.mcd.jmx.wrapper.JMXBeanOperation;
import br.com.ia.oct.mcd.jmx.wrapper.JMXBeanParameter;

/**
 * Servi�o JMX utilizado para administra��o da plataforma OCTOPUS
 * @author fernandosapata
 *
 */
@JMXBean(className = "SchedulerManagerService", description = "Administração do Scheduler da plataforma OCTOPUS")
@Singleton //(name="MCD.Management:service=SchedulerManager")
@Startup //(SchedulerManagerService.class)
public class SchedulerManagerMBean implements SchedulerManagerService {	

//	@EJB //(mappedName="octopus/mcd/management/scheduler/schedulermanager")
	@Inject
	private SchedulerManager scheduler;
	
//	@EJB   //(mappedName="octopus/mcd/management/dataservice")
	@Inject
	private DataService dataService;	
	
	private String schedulerManagerStatus = "";
	
	public boolean startup() {
		return scheduler.startup();
	}

	public boolean shutdown() {
		return scheduler.shutdown();
	}

	public boolean reload() {
		return scheduler.reload();
	}

	public boolean scheduleJob(Long id, String jndi, String grupo, String nome, String descricao, String cronTrigger, boolean persistent) {
		if (persistent) {
			// gravar na tabela utilizando o dataService
		}
		return scheduler.scheduleJob(id, jndi, grupo, nome, descricao, cronTrigger);
	}

	public void start() throws Exception {
		scheduler.startup();
	}

	public void destroy() throws Exception {
		scheduler.shutdown();		
	}

	public String getSchedulerManagerStatus() {
		schedulerManagerStatus = scheduler.shcedulerManagerStatus();
		return schedulerManagerStatus;
	}

	public boolean execJobNow(String uid) {
		return scheduler.execJobNow(uid);
	}

	public String jobList() {
		List<Job> jobs = dataService.getList(Job.class);
		
		StringBuffer retorno = new StringBuffer();
		retorno.append("<table>");
		retorno.append("<tr>");
		retorno.append("<th>ID</th>");
		retorno.append("<th>Produto</th>");
		retorno.append("<th>UID</th>");
		retorno.append("<th>Grupo</th>");
		retorno.append("<th>Nome</th>");
		retorno.append("<th>Ativo</th>");
		retorno.append("<th>Descrição</th>");
		retorno.append("<th>JNDI</th>");
		retorno.append("<th>Last Exec Status</th>");
		retorno.append("<th>Last Error Message</th>");
		retorno.append("<th>Last exec Time (ms)</th>");
		retorno.append("<th>Last Execution</th>");
		retorno.append("<th>Next Execution</th>");
		retorno.append("<th>Node</th>");			
		retorno.append("</tr>");
		
		for (Job j : jobs) {
			retorno.append("<tr>");
			retorno.append("<td>" + j.getId() + "</td>");
			retorno.append("<td>" + j.getProduto() + "</td>");
			retorno.append("<td>" + j.getUid() + "</td>");
			retorno.append("<td>" + j.getGrupo() + "</td>");
			retorno.append("<td>" + j.getNome() + "</td>");
			retorno.append("<td>" + j.getAtivo() + "</td>");
			retorno.append("<td>" + j.getDescricao() + "</td>");
			retorno.append("<td>" + j.getJndiLookup() + "</td>");
			retorno.append("<td>" + j.getLastExecutionStatus() + "</td>");
			retorno.append("<td>" + j.getLastErrorMessage() + "</td>");
			retorno.append("<td>" + j.getLastExecutionTime() + "</td>");
			
			if (j.getLastExecution() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + j.getLastExecution().getTime().toGMTString() + "</td>");
			}
			
			if (j.getNextExecution() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + j.getNextExecution().getTime().toGMTString() + "</td>");
			}
			
			retorno.append("<td>" + j.getNode() + "</td>");			
			retorno.append("</tr>");
		}
		
		retorno.append("</table>");

		System.out.println(retorno.toString());
		return retorno.toString() + "\n";
	}

	@JMXBeanOperation(name = "jobListByProduto", description = "Retorna a lista de JOBS filtrando pelo nome do produto")
	public String jobListByProduto(
			@JMXBeanParameter(name = "Produto", description = "Nome do produto") String produto) {
		
		Junction junction = Restrictions.conjunction()
				.add(Restrictions.eq("produto", produto));				
		
		List<Job> jobs = dataService.getList(Job.class, junction);
		
		StringBuffer retorno = new StringBuffer();
		retorno.append("<table>");
		retorno.append("<tr>");
		retorno.append("<th>ID</th>");
		retorno.append("<th>Produto</th>");
		retorno.append("<th>UID</th>");
		retorno.append("<th>Grupo</th>");
		retorno.append("<th>Nome</th>");
		retorno.append("<th>Ativo</th>");
		retorno.append("<th>Descrição</th>");
		retorno.append("<th>JNDI</th>");
		retorno.append("<th>Last Exec Status</th>");
		retorno.append("<th>Last Error Message</th>");
		retorno.append("<th>Last exec Time (ms)</th>");
		retorno.append("<th>Last Execution</th>");
		retorno.append("<th>Next Execution</th>");
		retorno.append("<th>Node</th>");			
		retorno.append("</tr>");
		
		for (Job j : jobs) {
			retorno.append("<tr>");
			retorno.append("<td>" + j.getId() + "</td>");
			retorno.append("<td>" + j.getProduto() + "</td>");
			retorno.append("<td>" + j.getUid() + "</td>");
			retorno.append("<td>" + j.getGrupo() + "</td>");
			retorno.append("<td>" + j.getNome() + "</td>");
			retorno.append("<td>" + j.getAtivo() + "</td>");
			retorno.append("<td>" + j.getDescricao() + "</td>");
			retorno.append("<td>" + j.getJndiLookup() + "</td>");
			retorno.append("<td>" + j.getLastExecutionStatus() + "</td>");
			retorno.append("<td>" + j.getLastErrorMessage() + "</td>");
			retorno.append("<td>" + j.getLastExecutionTime() + "</td>");
			
			if (j.getLastExecution() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + j.getLastExecution().getTime().toGMTString() + "</td>");
			}
			
			if (j.getNextExecution() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + j.getNextExecution().getTime().toGMTString() + "</td>");
			}
			
			retorno.append("<td>" + j.getNode() + "</td>");			
			retorno.append("</tr>");
		}
		
		retorno.append("</table>");

		System.out.println(retorno.toString());
		return retorno.toString() + "\n";
	}	
	
	public boolean updateJobStatus(Long id, boolean active) {
		Job job = (Job) dataService.getByID(id, Job.class);
		
		job.setAtivo(active);
		
		dataService.evict(Job.class);
		
		dataService.saveOrUpdate(job, Job.class);
		
		return true;
	}
	
	public String jobsFireTime() throws Exception {
		Scheduler sched = scheduler.getScheduler();
		
		StringBuffer retorno = new StringBuffer();
		retorno.append("<table>");
		retorno.append("<tr>");
		retorno.append("<th>ID</th>");
		retorno.append("<th>JNDI</th>");
		retorno.append("<th>FireTime</th>");
		retorno.append("<th>PreviousFireTime</th>");
		retorno.append("<th>NextFireTime</th>");
		retorno.append("<th>ScheduledFireTime</th>");
		retorno.append("<th>JobRunTime</th>");
		retorno.append("<th>isRecovering</th>");		
		retorno.append("</tr>");		
		
		List<JobExecutionContext> currentlyExecutingJobs = sched.getCurrentlyExecutingJobs();
		
		for (JobExecutionContext j : currentlyExecutingJobs) {
			retorno.append("<tr>");
			retorno.append("<td>" + j.getJobDetail().getJobDataMap().getString("id") + "</td>");
			retorno.append("<td>" + j.getJobDetail().getJobDataMap().getString("jndi") + "</td>");
			if (j.getFireTime() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + j.getFireTime().toLocaleString() + "</td>");
			}			
			if (j.getPreviousFireTime() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + j.getPreviousFireTime().toLocaleString() + "</td>");
			}			
			if (j.getNextFireTime() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + j.getNextFireTime().toLocaleString() + "</td>");
			}
			if (j.getScheduledFireTime() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + j.getScheduledFireTime().toLocaleString() + "</td>");
			}
			retorno.append("<td>" + j.getJobRunTime() + "</td>");
			retorno.append("<td>" + j.isRecovering() + "</td>");					
			retorno.append("</tr>");
			
		}
		
		retorno.append("</table>");

		System.out.println(retorno.toString());
		return retorno.toString() + "\n";
	}		

}
