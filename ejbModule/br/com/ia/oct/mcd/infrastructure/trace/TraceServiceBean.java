package br.com.ia.oct.mcd.infrastructure.trace;

import java.util.Calendar;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.ia.oct.mcd.common.annotations.OCTComponent;
import br.com.ia.oct.mcd.common.entity.Trace;
import br.com.ia.oct.mcd.common.utils.Configuration;
import br.com.ia.oct.mcd.dataservice.DataService;

/**
 * Componente utilizado para gerar traces no ambiente, pode ser utilizado para gerar os dados em banco e também enviar a informação para o Remedy
 * Verificar parâmetro [oct.management.trace.destino] no arquivo de configuração
 * Valores válidos [DB, REMEDY, BOTH]
 * @author fernandosapata
 *
 */
@Stateful
@Remote(TraceService.class)
@Local(TraceServiceLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/management/infrastructure/traceservice")
@EJB(name = "octopus/mcd/management/infrastructure/traceservice", beanInterface = TraceService.class) 
@OCTComponent(version="$LastChangedRevision$",uid="4.3.1", nome="TraceServiceBean", descricao="Service de TRACE da Plataforma OCTOPUS", jndiLookup="MCD/TraceServiceBean/local", clazz="", cronTrigger="0 0/5 * 1/1 * ? *", grupo="MANAGEMENT", ativo=true)
public class TraceServiceBean implements TraceServiceLocal, TraceService {
	private static final Logger LOG = Logger.getLogger(TraceServiceBean.class);
	
	//@EJB  //(mappedName="octopus/mcd/management/dataservice")
	@Inject
	private DataService<Trace> dataService; 
	
	public void trace (Trace trace) {
		String destino = Configuration.getConfig("mcd.management.trace.destino");
		
		if ("DB".equalsIgnoreCase(destino)) {
			try {
				LOG.debug("Trace recebido: [componentID=" + trace.getComponentID() + ",message=" + trace.getMessage() + ",details=" + trace.getDetails() + ",date=" + trace.getDate().getTime().toLocaleString() + "]");
				dataService.save(trace, Trace.class);
			} catch (Throwable t) {
				LOG.error(t.getMessage(), t);
			} 
		} else if ("REMEDY".equalsIgnoreCase(destino)) {
			// Implementar Remedy			
		} else if ("BOTH".equalsIgnoreCase(destino)) {
			try {
				dataService.save(trace, Trace.class);
			} catch (Throwable t) {
				LOG.error(t.getMessage(), t);
			}
			// Implementar Remedy
		}
	}

	public void trace (Long seqUid, String componentID, String message, String details, Calendar date) {		
		Trace trace = new Trace();
		trace.setSeqUid(seqUid);
		trace.setComponentID(componentID);
		trace.setMessage(message);
		trace.setDetails(details);
		trace.setDate(date);
	
		this.trace(trace);
	}

	public Boolean isAlive() {
		return true;
	}
}
