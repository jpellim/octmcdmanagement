package br.com.ia.oct.mcd.infrastructure.trace;


public interface TraceManagerService {
	public void start () throws Exception;
	public void destroy () throws Exception;	
	public String traceByComponenteID(String componentID, Integer limit);
	public String lastTraces(Integer limit);
	public String findByMessage(String query, Integer limit);
	public String messageByID(Long id);
}
