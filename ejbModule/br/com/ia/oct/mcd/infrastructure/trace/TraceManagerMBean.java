package br.com.ia.oct.mcd.infrastructure.trace;

import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ia.oct.mcd.common.entity.Trace;
import br.com.ia.oct.mcd.dataservice.DataService;

/**
 * Servi�o JMX utilizado para administra��o da plataforma OCTOPUS
 * @author fernandosapata
 *
 */
@Singleton //(name="MCD.Management:service=TraceManager")
@Startup //(TraceManagerService.class)
public class TraceManagerMBean implements TraceManagerService {	
//	@EJB //(mappedName="octopus/mcd/management/dataservice")
	
	@Inject
	private DataService dataService;

	public void start() throws Exception {

	}

	public void destroy() throws Exception {

	}

	public String traceByComponenteID(String componentID, Integer limit) {
		
		if (limit == null) {
			limit = 100;
		} else {
			if (limit.compareTo(0) == 0) {
				limit = 100;
			}			
		}		
		
		Junction junction = Restrictions.conjunction()
				.add(Restrictions.eq("componentID", componentID));		
		
		List<Trace> traces = dataService.getList(Trace.class, limit, Order.desc("id"), junction);
		
		StringBuffer retorno = new StringBuffer();
		retorno.append("<table>");

		retorno.append("<tr>");
		retorno.append("<th style=\"text-align: left;\" colspan=\"6\">Component ID: " + componentID + "</th>");
		retorno.append("</tr>");	
		retorno.append("<tr>");
		retorno.append("<th style=\"text-align: left;\" colspan=\"6\">Limit: " + limit + "</th>");
		retorno.append("</tr>");	
		
		retorno.append("<tr>");
		retorno.append("<th>ID</th>");
		retorno.append("<th>SeqUid</th>");
		retorno.append("<th>ComponentID</th>");
		retorno.append("<th>Message</th>");
		retorno.append("<th>Details</th>");
		retorno.append("<th>Date</th>");
		retorno.append("</tr>");			
		
		for (Trace t : traces) {
			retorno.append("<tr>");
			retorno.append("<td>" + t.getId() + "</td>");
			retorno.append("<td>" + t.getSeqUid() + "</td>");
			retorno.append("<td>" + t.getComponentID() + "</td>");
			retorno.append("<td>" + t.getMessage() + "</td>");

			if (t.getDetails().length() > 100) {
				String details = t.getDetails().substring(0, 97);
				details = StringEscapeUtils.escapeHtml(details);
				
				retorno.append("<td><pre>" + details + "...<pre></td>");
			} else {
				String details = t.getDetails();
				details = StringEscapeUtils.escapeHtml(details);
				
				retorno.append("<td><pre>" + details + "<pre></td>");	
			}	
			
			retorno.append("<td>" + t.getDate().getTime().toLocaleString() + "</td>");
			retorno.append("</tr>");
		}
		
		retorno.append("</table>");

		System.out.println(retorno.toString());
		return retorno.toString() + "\n";
	}

	public String lastTraces(Integer limit) {
		if (limit == null) {
			limit = 100;
		} else {
			if (limit.compareTo(0) == 0) {
				limit = 100;
			}			
		}		
		
		List<Trace> traces = dataService.getList(Trace.class, limit, Order.desc("id"));
		
		StringBuffer retorno = new StringBuffer();
		retorno.append("<table>");

		retorno.append("<tr>");
		retorno.append("<th style=\"text-align: left;\" colspan=\"6\">Limit: " + limit + "</th>");
		retorno.append("</tr>");	
		
		retorno.append("<tr>");
		retorno.append("<th>ID</th>");
		retorno.append("<th>SeqUid</th>");
		retorno.append("<th>ComponentID</th>");
		retorno.append("<th>Message</th>");
		retorno.append("<th>Details</th>");
		retorno.append("<th>Date</th>");
		retorno.append("</tr>");			
		
		for (Trace t : traces) {
			retorno.append("<tr>");
			retorno.append("<td>" + t.getId() + "</td>");
			retorno.append("<td>" + t.getSeqUid() + "</td>");
			retorno.append("<td>" + t.getComponentID() + "</td>");
			retorno.append("<td>" + t.getMessage() + "</td>");
			
			if (t.getDetails().length() > 100) {
				String details = t.getDetails().substring(0, 97);
				details = StringEscapeUtils.escapeHtml(details);
				
				retorno.append("<td><pre>" + details + "...<pre></td>");
			} else {
				String details = t.getDetails();
				details = StringEscapeUtils.escapeHtml(details);
				
				retorno.append("<td><pre>" + details + "<pre></td>");	
			}	
			
			retorno.append("<td>" + t.getDate().getTime().toLocaleString() + "</td>");
			retorno.append("</tr>");
		}
		
		retorno.append("</table>");

		System.out.println(retorno.toString());
		return retorno.toString() + "\n";
	}

	public String findByMessage(String query, Integer limit) {
		if (limit == null) {
			limit = 100;
		} else {
			if (limit.compareTo(0) == 0) {
				limit = 100;
			}			
		}		
		
		Junction junction = Restrictions.disjunction()
				.add(Restrictions.like("message", query, MatchMode.ANYWHERE))
				.add(Restrictions.like("details", query, MatchMode.ANYWHERE));
		
		List<Trace> traces = dataService.getList(Trace.class, limit, Order.desc("id"), junction);
		
		StringBuffer retorno = new StringBuffer();
		retorno.append("<table>");

		retorno.append("<tr>");
		retorno.append("<th style=\"text-align: left;\" colspan=\"6\">Query: " + query + "</th>");
		retorno.append("</tr>");	
		retorno.append("<tr>");
		retorno.append("<th style=\"text-align: left;\" colspan=\"6\">Limit: " + limit + "</th>");
		retorno.append("</tr>");	
		
		retorno.append("<tr>");
		retorno.append("<th>ID</th>");
		retorno.append("<th>SeqUid</th>");
		retorno.append("<th>ComponentID</th>");
		retorno.append("<th>Message</th>");
		retorno.append("<th>Details</th>");
		retorno.append("<th>Date</th>");
		retorno.append("</tr>");			
		
		for (Trace t : traces) {
			retorno.append("<tr>");
			retorno.append("<td>" + t.getId() + "</td>");
			retorno.append("<td>" + t.getSeqUid() + "</td>");
			retorno.append("<td>" + t.getComponentID() + "</td>");
			retorno.append("<td>" + t.getMessage() + "</td>");

			if (t.getDetails().length() > 100) {
				String details = t.getDetails().substring(0, 97);
				details = StringEscapeUtils.escapeHtml(details);
				
				retorno.append("<td><pre>" + details + "...<pre></td>");
			} else {
				String details = t.getDetails();
				details = StringEscapeUtils.escapeHtml(details);
				
				retorno.append("<td><pre>" + details + "<pre></td>");	
			}	
			
			retorno.append("<td>" + t.getDate().getTime().toLocaleString() + "</td>");
			retorno.append("</tr>");
		}
		
		retorno.append("</table>");

		System.out.println(retorno.toString());
		return retorno.toString() + "\n";
	}

	public String messageByID(Long id) {
		Trace trace = (Trace) dataService.getByID(id, Trace.class);
		
		StringBuffer retorno = new StringBuffer();
		retorno.append("<table width=\"100%\">");
		
		retorno.append("<tr>");
		retorno.append("<th>ID</th>");
		retorno.append("<td>" + trace.getId() + "</td>");
		retorno.append("</tr>");			
		retorno.append("<tr>");
		retorno.append("<th>SeqUid</th>");
		retorno.append("<td>" + trace.getSeqUid() + "</td>");
		retorno.append("</tr>");
		retorno.append("<tr>");
		retorno.append("<th>Component ID</th>");
		retorno.append("<td>" + trace.getComponentID() + "</td>");
		retorno.append("</tr>");	
		retorno.append("<tr>");
		retorno.append("<th>Date</th>");
		retorno.append("<td>" + trace.getDate().getTime().toLocaleString() + "</td>");
		retorno.append("</tr>");
		retorno.append("<tr>");
		retorno.append("<th>Message</th>");
		retorno.append("<td>" + trace.getMessage() + "</td>");
		retorno.append("</tr>");	
		retorno.append("<tr>");
		retorno.append("<th style=\"text-align: left;\" colspan=\"2\">Details</th>");
		retorno.append("</tr>");	
		retorno.append("<tr>");
		retorno.append("<td colspan=\"2\"><textarea rows=\"20\" style=\"width: 99%;\">" + trace.getDetails() + "</textarea></td>");
		retorno.append("</tr>");	
		
		retorno.append("</table>");
		
		System.out.println(retorno.toString());
		return retorno.toString() + "\n";
	}	


}
