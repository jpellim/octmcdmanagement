package br.com.ia.oct.mcd.infrastructure.sequence;

import java.util.Calendar;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.inject.Inject;

import br.com.ia.oct.mcd.common.entity.SeqUid;
import br.com.ia.oct.mcd.dataservice.DataService;

/**
 * Classe utilizada para gerar uma sequencia de IDs para identifica��o da transa��o dentro do ambiente.
 * @author fernandosapata
 *
 */
@Stateful
@Remote(SequenceService.class)
@Local(SequenceServiceLocal.class)
//@RemoteBinding(jndiBinding="octopus/mcd/management/infrastructure/sequenceservice")
@EJB(name = "octopus/mcd/management/infrastructure/sequenceservice", beanInterface = SequenceService.class)  
public class SequenceServiceBean implements SequenceServiceLocal, SequenceService {
//	@EJB  //(mappedName="octopus/mcd/management/dataservice")
	
	@Inject
	private DataService<SeqUid> dataService; 
	
	public Long getUid () {
		SeqUid uid = new SeqUid();
		uid.setData(Calendar.getInstance());
		
		uid = dataService.saveObj(uid, SeqUid.class);
		
		return uid.getId();
	}
}
