package br.com.ia.oct.mcd.loader;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Restrictions;

import br.com.ia.oct.mcd.common.entity.Component;
import br.com.ia.oct.mcd.common.entity.Job;
import br.com.ia.oct.mcd.common.utils.Configuration;
import br.com.ia.oct.mcd.dataservice.DataService;
import br.com.ia.oct.mcd.mgmt.register.MgmtRegister;
import br.com.ia.oct.mcd.remedy.register.RemedyRegister;
import br.com.ia.oct.mcd.scheduler.SchedulerManager;

/**
 * Componente utilizado para centralizar os eventos de neg�cio do sistema em um reposit�rio �nico
 * @author fernandosapata
 * @ID: 4.3.1
 */
@Singleton //(name="MCD.Management:service=LoaderService")
@Startup  //(OCTLoaderService.class)
public class OCTLoaderMBean implements OCTLoaderService {

	//@EJB  //(mappedName="octopus/mcd/management/register")
	@Inject
	private MgmtRegister mgmtRegister;	
	
	//@EJB //(mappedName="octopus/mcd/management/dataservice")
	@Inject
	private DataService dataService;		
	
	//@EJB //(mappedName="octopus/mcd/management/scheduler/schedulermanager")
	@Inject
	private SchedulerManager schedulerManager;
	
	//@EJB //(mappedName="octopus/mcd/remedy/register")
	@Inject
	private RemedyRegister remedyRegister ;		
	
	public void start() throws Exception {
		this.doRegister();
	}

	public void destroy() throws Exception {
	}

	public void registerComponents(Boolean force) throws Exception {
		schedulerManager.shutdown();
		
		if (force) {
			mgmtRegister.truncateComponents();
		}
		this.doRegister();
		
		schedulerManager.startup();
	}

	private void doRegister () throws Exception {
		mgmtRegister.doRegister();	
		remedyRegister.doRegister();
	}
	
	public String jobList(String produto) {
		List<Job> jobs;
		
		if (produto == "") {
			jobs = dataService.getList(Job.class);
		} else {
			Junction junction = Restrictions.conjunction()
					.add(Restrictions.eq("produto", produto));
			
			jobs = dataService.getList(Job.class, junction);			
		}		
		
		StringBuffer retorno = new StringBuffer();
		retorno.append("<table>");
		retorno.append("<tr>");
		retorno.append("<th>ID</th>");
		retorno.append("<th>Produto</th>");
		retorno.append("<th>UID</th>");
		retorno.append("<th>Grupo</th>");
		retorno.append("<th>Nome</th>");
		retorno.append("<th>Ativo</th>");
		retorno.append("<th>Descrição</th>");
		retorno.append("<th>JNDI</th>");
		retorno.append("<th>Last Exec Status</th>");
		retorno.append("<th>Last Error Message</th>");
		retorno.append("<th>Last exec Time (ms)</th>");
		retorno.append("<th>Last Execution</th>");
		retorno.append("<th>Next Execution</th>");
		retorno.append("<th>Node</th>");			
		retorno.append("</tr>");
		
		for (Job j : jobs) {
			retorno.append("<tr>");
			retorno.append("<td>" + j.getId() + "</td>");
			retorno.append("<td>" + j.getProduto() + "</td>");
			retorno.append("<td>" + j.getUid() + "</td>");
			retorno.append("<td>" + j.getGrupo() + "</td>");
			retorno.append("<td>" + j.getNome() + "</td>");
			retorno.append("<td>" + j.getAtivo() + "</td>");
			retorno.append("<td>" + j.getDescricao() + "</td>");
			retorno.append("<td>" + j.getJndiLookup() + "</td>");
			retorno.append("<td>" + j.getLastExecutionStatus() + "</td>");
			retorno.append("<td>" + j.getLastErrorMessage() + "</td>");
			retorno.append("<td>" + j.getLastExecutionTime() + "</td>");
			
			if (j.getLastExecution() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + j.getLastExecution().getTime().toGMTString() + "</td>");
			}
			
			if (j.getNextExecution() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + j.getNextExecution().getTime().toGMTString() + "</td>");
			}
			
			retorno.append("<td>" + j.getNode() + "</td>");			
			retorno.append("</tr>");
		}
		
		retorno.append("</table>");

		System.out.println(retorno.toString());
		return retorno.toString() + "\n";
	}

	public String componentList(String produto) {
		List<Component> components;
		
		if (produto == "") {
			components = dataService.getList(Component.class);
		} else {
			Junction junction = Restrictions.conjunction()
					.add(Restrictions.eq("produto", produto));
			
			components = dataService.getList(Component.class, junction);			
		}		
		
		StringBuffer retorno = new StringBuffer();
		retorno.append("<table>");
		retorno.append("<tr>");
		retorno.append("<th>ID</th>");
		retorno.append("<th>Produto</th>");
		retorno.append("<th>UID</th>");
		retorno.append("<th>Version</th>");
		retorno.append("<th>Grupo</th>");
		retorno.append("<th>Nome</th>");
		retorno.append("<th>Ativo</th>");
		retorno.append("<th>Descrição</th>");
		retorno.append("<th>JNDI</th>");
		retorno.append("<th>Status</th>");
		retorno.append("<th>Last Error Message</th>");
		retorno.append("<th>Duration (ms)</th>");
		retorno.append("<th>Last Ping Time (ms)</th>");
		retorno.append("<th>Node</th>");			
		retorno.append("</tr>");
		
		for (Component c : components) {
			retorno.append("<tr>");
			retorno.append("<td>" + c.getId() + "</td>");
			retorno.append("<td>" + c.getProduto() + "</td>");
			retorno.append("<td>" + c.getUid() + "</td>");
			retorno.append("<td>" + c.getcVersion() + "</td>");
			retorno.append("<td>" + c.getGrupo() + "</td>");
			retorno.append("<td>" + c.getNome() + "</td>");
			retorno.append("<td>" + c.getAtivo() + "</td>");
			retorno.append("<td>" + c.getDescricao() + "</td>");
			retorno.append("<td>" + c.getJndiLookup() + "</td>");
			retorno.append("<td>" + c.getStatus() + "</td>");
			retorno.append("<td>" + c.getLastErrorMessage() + "</td>");
			retorno.append("<td>" + c.getDuration() + "</td>");
			
			if (c.getLastPingTime() == null) {
				retorno.append("<td>&nbsp;</td>");
			} else {
				retorno.append("<td>" + c.getLastPingTime().getTime().toGMTString() + "</td>");
			}
			
			retorno.append("<td>" + c.getNode() + "</td>");			
			retorno.append("</tr>");
		}
		
		retorno.append("</table>");

		System.out.println(retorno.toString());
		return retorno.toString() + "\n";
	}

	public void reloadPropertiesFile() {
		Configuration.loadBundle();		
	}	
}
