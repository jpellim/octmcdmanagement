package br.com.ia.oct.mcd.loader;

public interface OCTLoaderService {
	public void start() throws Exception;
	public void destroy() throws Exception;
	public void registerComponents(Boolean force) throws Exception;
	public String jobList(String produto);
	public String componentList(String produto);
	public void reloadPropertiesFile();
}
